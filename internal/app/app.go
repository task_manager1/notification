package app

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/task_manager1/notification/docs"
	"gitlab.com/task_manager1/notification/internal/config"
	httpDelivery "gitlab.com/task_manager1/notification/internal/delivery/http"
	"gitlab.com/task_manager1/notification/internal/repository"
	"gitlab.com/task_manager1/notification/internal/server"
	"gitlab.com/task_manager1/notification/internal/service"
	"gitlab.com/task_manager1/notification/pkg/db/migrate"
	"gitlab.com/task_manager1/notification/pkg/db/postgresql"
	"gitlab.com/task_manager1/notification/pkg/openapi"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func Run(cfg *config.Config) {
	if cfg.Service.Domain == "127.0.0.1" || cfg.Service.Domain == "localhost" {
		docs.SwaggerInfo.Host = fmt.Sprintf("localhost:%s", cfg.Service.Port)
		docs.SwaggerInfo.Schemes = []string{"http", "https"}
	} else {
		docs.SwaggerInfo.Host = cfg.Service.Domain
		docs.SwaggerInfo.Schemes = []string{"https", "http"}
		err := openapi.NewOpenApiClient(
			cfg.Service.AppName,
			cfg.Service.OpenapiEndpoint,
			docs.SwaggerInfo.ReadDoc(),
		).Send(context.Background()).Error()
		if err != nil {
			fmt.Print(fmt.Errorf(err.Error()))
		}
	}

	connection, db, err := postgresql.NewDB(cfg.Database.WriteDSNs, cfg.Database.ReadDSNs)
	if err != nil {
		fmt.Print(fmt.Errorf(err.Error()))
	}

	if cfg.Service.RunMigration {
		if err := migrate.Up(connection, cfg.Database.Schema, "migrations"); err != nil {
			fmt.Print(fmt.Errorf(err.Error()))
		}
	}

	repos := repository.NewRepositories(db)
	var services = service.NewServices(service.Deps{
		Repos: repos,
		Cgf:   cfg,
	})

	handlerDelivery := httpDelivery.NewHandlerDelivery(services, cfg.Service.AppName)

	// HTTP Server
	srv, err := server.NewServer(cfg, handlerDelivery)
	if err != nil {
		fmt.Print(fmt.Errorf(err.Error()))
	}

	go func() {
		if err := srv.Run(); !errors.Is(err, http.ErrServerClosed) {
			fmt.Print("🔥 Server stopped due error", "err", err.Error())
		} else {
			fmt.Print("✅ Server shutdown successfully")
		}
	}()

	fmt.Printf(fmt.Sprintf("🚀 Starting server at http://0.0.0.0:%s", cfg.Service.Port))

	grpcSrv, err := server.NewGRPCServer(cfg, services)
	if err != nil {
		fmt.Print(fmt.Errorf(err.Error()))
	}

	go func() {
		if err = grpcSrv.Run(); !errors.Is(err, http.ErrServerClosed) {
			fmt.Print("🔥 Server stopped due error", "err", err.Error())
		} else {
			fmt.Print("✅ Server shutdown successfully")
		}
	}()

	fmt.Printf(fmt.Sprintf("🚀 Starting gRPC server at http://0.0.0.0:%s", cfg.Service.GrpcPort))

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)

	<-quit
	if err = connection.Close(); err != nil {
		fmt.Print(fmt.Errorf(err.Error()))
	}
}

func connectToGrpcServer(address string) (*grpc.ClientConn, error) {
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	}

	conn, err := grpc.Dial(address, opts...)
	if err != nil {
		return nil, err
	}

	return conn, nil
}
