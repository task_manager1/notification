package middleware

import (
	"context"
	"github.com/gin-gonic/gin"
	"strconv"
)

type PaginationInfo struct {
	PerPage    int
	Page       int
	Pagination bool
	Total      int64
}

const PaginationCtx string = "pagination"

func Pagination() gin.HandlerFunc {
	return func(c *gin.Context) {
		perPagePath := c.Query("per_page")
		pagePath := c.Query("page")
		if perPagePath == "" {
			perPagePath = "0"
		}
		if pagePath == "" {
			pagePath = "0"
		}
		if perPagePath == "0" && pagePath == "0" {
			ctx := context.WithValue(c.Request.Context(), PaginationCtx, PaginationInfo{})
			c.Request = c.Request.WithContext(ctx)
			return
		}
		page, err := strconv.Atoi(pagePath)
		if err != nil {
			ctx := context.WithValue(c.Request.Context(), PaginationCtx, PaginationInfo{})
			c.Request = c.Request.WithContext(ctx)
			return
		}
		perPage, err := strconv.Atoi(perPagePath)
		if err != nil {
			ctx := context.WithValue(c.Request.Context(), PaginationCtx, PaginationInfo{})
			c.Request = c.Request.WithContext(ctx)
			return
		}
		if page == 0 || page <= 0 {
			page = 1
		}
		if perPage > 100 || perPage <= 0 {
			perPage = 100
		}
		p := PaginationInfo{
			PerPage:    perPage,
			Page:       page,
			Pagination: true,
		}
		ctx := context.WithValue(c.Request.Context(), PaginationCtx, p)
		c.Request = c.Request.WithContext(ctx)
		return
	}
}

func (p PaginationInfo) SetHeader(c *gin.Context) {
	if !p.Pagination {
		return
	}

	c.Header("x-page", strconv.Itoa(p.Page))
	c.Header("x-per-page", strconv.Itoa(p.PerPage))
	c.Header("x-total", strconv.FormatInt(p.Total, 16))
}
