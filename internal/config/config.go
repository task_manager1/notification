package config

import (
	"fmt"
	_ "github.com/hashicorp/vault/api"
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"sync"
)

type (
	Config struct {
		Service   *Service
		Worker    *Worker
		Database  *Database
		GrpcConns *GrpcConnections
	}

	Service struct {
		AppName         string         `envconfig:"APP_NAME" required:"true"`
		Environment     AppEnvironment `envconfig:"ENVIRONMENT" default:"local"`
		Port            string         `envconfig:"PORT" default:"3000"`
		GrpcPort        string         `envconfig:"GRPC_PORT" default:"4040"`
		Domain          string         `envconfig:"DOMAIN" default:"localhost"`
		Limiter         string         `envconfig:"LIMITER_SETTINGS"`
		OpenapiEndpoint string         `envconfig:"OPENAPI_ENDPOINT" required:"false"`
		RunMigration    bool           `envconfig:"RUN_MIGRATION"`
	}

	Worker struct {
		RunEchoWorker bool `envconfig:"RUN_ECHO_WORKER" default:"true"`
	}

	Database struct {
		ReadDSNs  []string `envconfig:"DATABASE_READ_DSN"`
		WriteDSNs []string `envconfig:"DATABASE_WRITE_DSN" required:"true"`
		Schema    string   `envconfig:"DATABASE_SCHEMA" required:"true"`
	}

	GrpcConnections struct {
		Auth string `envconfig:"AUTH_URL" default:"auth:4040"`
	}
)

var (
	once   sync.Once
	config *Config
)

// GetConfig Загружает конфиг из .env файла и возвращает объект конфигурации
// В случае, если не передать параметр `envfiles`, берется `.env` файл из корня проекта
func GetConfig(envfiles ...string) (*Config, error) {
	var err error
	once.Do(func() {
		_ = godotenv.Load(envfiles...)

		var c Config
		err = envconfig.Process("", &c)
		if err != nil {
			err = fmt.Errorf("error parse config from env variables: %w\n", err)
			return
		}

		if e := c.Service.Environment.Validate(); e != nil {
			err = fmt.Errorf("error parse config from env variables: %w\n", e)
			return
		}
		config = &c
	})

	return config, err
}

type AppEnvironment string

const (
	PRODUCTION  AppEnvironment = "prod"
	STAGE       AppEnvironment = "stage"
	DEVELOPMENT AppEnvironment = "dev"
	LOCAL       AppEnvironment = "local"
)

func (e AppEnvironment) IsProduction() bool {
	return e == PRODUCTION
}

func (e AppEnvironment) IsStage() bool {
	return e == STAGE
}

func (e AppEnvironment) IsDevelopment() bool {
	return e == DEVELOPMENT
}

func (e AppEnvironment) IsLocal() bool {
	return e == LOCAL
}

func (e AppEnvironment) String() string {
	return string(e)
}

func (e AppEnvironment) Validate() error {
	switch e {
	case LOCAL, DEVELOPMENT, STAGE, PRODUCTION:
		return nil
	default:
		return fmt.Errorf("unexpected ENVIRONMENT in .env: %s", e)
	}
}
