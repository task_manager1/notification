package v1

import (
	"context"
	"gitlab.com/task_manager1/notification/internal/model"
	"gitlab.com/task_manager1/notification/internal/service"
	"gitlab.com/task_manager1/proto/gorpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type NotificationServer struct {
	services *service.Services
	gorpc.UnimplementedNotificationServer
}

func NewNotificationServer(services *service.Services) *NotificationServer {
	return &NotificationServer{
		services: services,
	}
}

func (a *NotificationServer) CreateNotification(
	ctx context.Context,
	v1 *gorpc.CreateNotificationReqDataV1,
) (*gorpc.CreateNotificationResDataV1, error) {
	err := a.services.Notification.Create(ctx, model.Notification{
		Title:  v1.GetTitle(),
		Text:   v1.GetText(),
		TaskId: v1.GetTaskId(),
		UserId: v1.GetUserId(),
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	return &gorpc.CreateNotificationResDataV1{
		IsReceived: true,
	}, nil
}
