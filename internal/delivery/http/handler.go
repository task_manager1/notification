package http

import (
	"fmt"
	"github.com/Depado/ginprom"
	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/ulule/limiter/v3"
	ginLimiter "github.com/ulule/limiter/v3/drivers/middleware/gin"
	"github.com/ulule/limiter/v3/drivers/store/memory"
	"gitlab.com/task_manager1/notification/internal/common/middleware"
	"net/http"
	"time"

	"gitlab.com/task_manager1/notification/internal/config"
	v1 "gitlab.com/task_manager1/notification/internal/delivery/http/v1"
	"gitlab.com/task_manager1/notification/internal/service"
)

type Handler struct {
	services *service.Services
	baseUrl  string
}

func NewHandlerDelivery(
	services *service.Services,
	baseUrl string,
) *Handler {
	return &Handler{
		services: services,
		baseUrl:  baseUrl,
	}
}

func (h *Handler) Init(cfg *config.Config) (*gin.Engine, error) {
	if !cfg.Service.Environment.IsLocal() {
		gin.SetMode(gin.ReleaseMode)
	}

	app := gin.New()
	prom := ginprom.New(
		ginprom.Engine(app),
		ginprom.Path("/actuator/prometheus"),
		ginprom.Subsystem(""),
		ginprom.Namespace(""),
	)

	app.Use(
		middleware.New(
			middleware.WithLogRequestBody(cfg.Service.Environment.IsLocal()),
			middleware.WithLogResponseBody(cfg.Service.Environment.IsLocal()),
		),
		func(c *gin.Context) {
			ctx := sentry.SetHubOnContext(c.Request.Context(), sentry.CurrentHub().Clone())
			c.Request = c.Request.WithContext(ctx)
			c.Next()
		},
		sentrygin.New(sentrygin.Options{
			Repanic:         true,
			WaitForDelivery: true,
			Timeout:         time.Second,
		}),
		prom.Instrument(),
	)

	if err := h.applyLimiter(cfg.Service.Limiter, app); err != nil {
		return nil, fmt.Errorf("can't apply limiter: %w", err)
	}

	h.initAPI(app)

	return app, nil
}

func (h *Handler) applyLimiter(limiterSettings string, router *gin.Engine) error {
	if limiterSettings != "" {
		rate, err := limiter.NewRateFromFormatted(limiterSettings)
		if err != nil {
			return err
		}

		limit := limiter.New(
			memory.NewStore(),
			rate,
			limiter.WithTrustForwardHeader(true),
		)

		router.Use(
			ginLimiter.NewMiddleware(limit, ginLimiter.WithLimitReachedHandler(func(c *gin.Context) {
				c.AbortWithStatus(http.StatusTooManyRequests)
			})),
		)
	}

	return nil
}

func (h *Handler) initAPI(router *gin.Engine) {
	baseUrl := router.Group(h.baseUrl)

	router.GET(h.baseUrl+"-docs/docs/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	handlerV1 := v1.NewHandler(h.services)
	api := baseUrl.Group("/api")
	{
		handlerV1.Init(api)
	}
}
