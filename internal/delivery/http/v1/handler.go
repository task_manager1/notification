package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/task_manager1/notification/internal/common/middleware"
	"gitlab.com/task_manager1/notification/internal/service"
)

type Handler struct {
	services *service.Services
}

func NewHandler(services *service.Services) *Handler {
	return &Handler{
		services: services,
	}
}

func (h *Handler) Init(api *gin.RouterGroup) {
	v1 := api.Group("/v1")
	{
		h.initNotificationHandler(v1)
	}
}

func (h *Handler) getPagination(c *gin.Context) middleware.PaginationInfo {
	pagination, ok := c.Request.Context().Value(middleware.PaginationCtx).(middleware.PaginationInfo)
	if !ok {
		pagination = middleware.PaginationInfo{}
	}
	return pagination
}
