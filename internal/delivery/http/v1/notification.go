package v1

import (
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"gitlab.com/task_manager1/notification/internal/common/middleware"
	"gitlab.com/task_manager1/notification/internal/model"
	"gitlab.com/task_manager1/notification/internal/schema"
	"log"
	"net/http"
	"strconv"
)

var (
	clients  = make(map[*websocket.Conn]bool)
	upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool { return true },
	}
)

func (h *Handler) initNotificationHandler(v1 *gin.RouterGroup) {
	v1.PUT("/notification", middleware.GinErrorHandle(h.MarkAsRead))
	v1.GET("/notifications", middleware.GinErrorHandle(h.GetAllNotifications))
	v1.GET("/ws", h.WebsocketHandler)
}

// MarkAsRead
// WhoAmi godoc
// @Summary Отметить уведомление прочитанным
// @Accept json
// @Produce json
// @Param id query uint64 true "Notification ID"
// @Success 200 {object} schema.Response[schema.Empty]
// @Failure 400 {object} schema.Response[schema.Empty]
// @tags notifications
// @Router /api/v1/notification [put]
func (h *Handler) MarkAsRead(c *gin.Context) error {
	ctx := c.Request.Context()
	rawId := c.Query("id")
	id, err := strconv.ParseUint(rawId, 10, 64)
	if err != nil {
		return err
	}
	err = h.services.Notification.Delete(ctx, id)
	if err != nil {
		return err
	}

	return schema.Respond("success", c)
}

// GetAllNotifications
// WhoAmi godoc
// @Summary Получение всех уведомлении
// @Accept json
// @Produce json
// @Success 200 {object} schema.Response[[]model.Notification]
// @Failure 400 {object} schema.Response[schema.Empty]
// @tags notifications
// @Router /api/v1/notifications [get]
func (h *Handler) GetAllNotifications(c *gin.Context) error {
	ctx := c.Request.Context()
	tasks, err := h.services.Notification.GetAll(ctx)
	if err != nil {
		return err
	}
	return schema.Respond(tasks, c)
}

func sendUpdateNotification() {
	notification := model.Notification{ID: 0, Title: "Update", Text: "Data update required"}

	for client := range clients {
		err := client.WriteJSON(notification)
		if err != nil {
			log.Println("Error writing message to WebSocket:", err)
			client.Close()
			delete(clients, client)
		}
	}
}
