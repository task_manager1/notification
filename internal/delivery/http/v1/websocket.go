package v1

import (
	"github.com/gin-gonic/gin"
	"log"
)

func (h *Handler) WebsocketHandler(c*gin.Context) {
	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println("Error upgrading to WebSocket:", err)
		return
	}

	clients[conn] = true

	// Закрытие соединения и удаление клиента при отключении

	sendUpdateNotification()
}
