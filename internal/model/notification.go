package model

type Notification struct {
	TimestampMixin
	ID     uint64 `gorm:"column:id" json:"id"`
	Title  string `gorm:"column:title" json:"title"`
	Text   string `gorm:"column:text" json:"text"`
	TaskId uint64 `gorm:"column:task_id" json:"task_id"`
	UserId uint64 `gorm:"column:user_id" json:"user_id"`
	IsRead bool   `gorm:"column:is_read" json:"is_read"`
}

func (a Notification) TableName() string {
	return "notification.notifications"
}
