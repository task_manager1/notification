package database

import (
	"context"
	"gitlab.com/task_manager1/notification/internal/model"
)

type Notificationer interface {
	Create(ctx context.Context, notification model.Notification) (err error)
	Delete(ctx context.Context, id uint64) (err error)
	Update(ctx context.Context, notification model.Notification) (err error)
	GetAll(ctx context.Context) (notifications []model.Notification, err error)
}
