package database

import (
	"context"
	"gitlab.com/task_manager1/notification/internal/model"
	"gorm.io/gorm"
)

type NotificationDB struct {
	db *gorm.DB
}

func NewNotificationDB(
	db *gorm.DB,
) *NotificationDB {
	return &NotificationDB{
		db: db,
	}
}

func (r *NotificationDB) Create(ctx context.Context, notification model.Notification) (err error) {
	err = r.db.WithContext(ctx).Model(&model.Notification{}).Create(&notification).Error
	if err != nil {
		return err
	}

	return nil
}

func (r *NotificationDB) Delete(ctx context.Context, id uint64) (err error) {
	err = r.db.WithContext(ctx).Model(&model.Notification{}).Delete(model.Notification{}, id).Error
	if err != nil {
		return err
	}

	return nil
}

func (r *NotificationDB) Update(ctx context.Context, notification model.Notification) (err error) {
	err = r.db.WithContext(ctx).Model(&model.Notification{}).Where("id = ?", notification.ID).
		Updates(model.Notification{
			IsRead: notification.IsRead}).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *NotificationDB) GetAll(ctx context.Context) (notifications []model.Notification, err error) {
	err = r.db.WithContext(ctx).Model(&model.Notification{}).Find(&notifications).Error
	if err != nil {
		return []model.Notification{}, err
	}
	return notifications, nil
}
