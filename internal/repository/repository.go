package repository

import (
	"gitlab.com/task_manager1/notification/internal/repository/database"
	"gorm.io/gorm"
)

type Repositories struct {
	NotificationDB database.Notificationer
}

func NewRepositories(db *gorm.DB) *Repositories {
	notificationDB := database.NewNotificationDB(db)

	return &Repositories{
		NotificationDB: notificationDB,
	}
}
