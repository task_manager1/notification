package server

import (
	"fmt"
	"gitlab.com/task_manager1/notification/internal/config"
	grpcDeliveryV1 "gitlab.com/task_manager1/notification/internal/delivery/grpc/v1"
	"gitlab.com/task_manager1/notification/internal/service"
	"gitlab.com/task_manager1/proto/gorpc"
	"google.golang.org/grpc"
	"net"
)

type GrpcServer struct {
	server   *grpc.Server
	port     string
	services *service.Services
}

// NewGRPCServer creates a new gRPC server instance.
func NewGRPCServer(cfg *config.Config, services *service.Services) (*GrpcServer, error) {
	opts := make([]grpc.ServerOption, 0)

	return &GrpcServer{
		server:   grpc.NewServer(opts...),
		port:     cfg.Service.GrpcPort,
		services: services,
	}, nil
}

// Run runs the gRPC server.
func (gs *GrpcServer) Run() error {
	defer gs.server.GracefulStop()

	listener, err := net.Listen("tcp", fmt.Sprintf(":%s", gs.port))
	if err != nil {
		return fmt.Errorf("failed to listen gRPC port (%s): %v", gs.port, err)
	}

	// TODO register the gRPC servers here:
	gorpc.RegisterNotificationServer(gs.server, grpcDeliveryV1.NewNotificationServer(gs.services))

	if err = gs.server.Serve(listener); err != nil {
		return err
	}

	return nil
}
