package service

import (
	"context"
	"gitlab.com/task_manager1/notification/internal/model"
	"gitlab.com/task_manager1/notification/internal/repository"
)

type NotificationService struct {
	repos *repository.Repositories
}

func NewNotificationService(
	repos *repository.Repositories,
) *NotificationService {
	return &NotificationService{
		repos: repos,
	}
}

func (s *NotificationService) Create(ctx context.Context, notification model.Notification) (err error) {
	err = s.repos.NotificationDB.Create(ctx, notification)
	if err != nil {
		return err
	}
	return nil
}

func (s *NotificationService) Update(ctx context.Context, notification model.Notification) (err error) {
	err = s.repos.NotificationDB.Update(ctx, notification)
	if err != nil {
		return err
	}
	return nil
}

func (s *NotificationService) Delete(ctx context.Context, id uint64) (err error) {
	err = s.repos.NotificationDB.Delete(ctx, id)
	if err != nil {
		return err
	}
	return nil
}

func (s *NotificationService) GetAll(ctx context.Context) (tasks []model.Notification, err error) {
	tasks, err = s.repos.NotificationDB.GetAll(ctx)
	if err != nil {
		return []model.Notification{}, err
	}
	return tasks, nil
}
