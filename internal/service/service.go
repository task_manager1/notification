package service

import (
	"context"
	"gitlab.com/task_manager1/notification/internal/config"
	"gitlab.com/task_manager1/notification/internal/model"
	"gitlab.com/task_manager1/notification/internal/repository"
)

type Notification interface {
	Create(ctx context.Context, task model.Notification) (err error)
	Update(ctx context.Context, task model.Notification) (err error)
	Delete(ctx context.Context, id uint64) (err error)
	GetAll(ctx context.Context) (tasks []model.Notification, err error)
}

type Services struct {
	Notification Notification
}

type Deps struct {
	Repos *repository.Repositories
	Cgf   *config.Config
}

func NewServices(deps Deps) *Services {
	notificationService := NewNotificationService(deps.Repos)

	return &Services{
		Notification: notificationService,
	}
}
