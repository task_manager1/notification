package main

import (
	"embed"
	"gitlab.com/task_manager1/notification/internal/app"
	"gitlab.com/task_manager1/notification/internal/config"
	"gitlab.com/task_manager1/notification/pkg/db/migrate"
)

//go:generate swag init

//go:embed migrations/*.sql
var embedMigrations embed.FS

// @title notification
// @версия 1.0.0
// @description notification
//
// @host task_manager-dev.kz
// @BasePath /notification
// @schemes https http
func main() {
	cfg, err := config.GetConfig()
	if err != nil {
		panic(err)
	}

	migrate.SetBaseFS(embedMigrations)

	app.Run(cfg)
}
